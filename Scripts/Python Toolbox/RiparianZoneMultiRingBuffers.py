import arcpy


class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Riparian Toolbox"
        self.alias = "RT"

        # List of tool classes associated with this toolbox
        self.tools = [RiparianZoneBuffers]


class RiparianZoneBuffers(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Riparian Zone Multi Ring Buffers"
        self.description = "Create multiple buffer rings around a designated body of water"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        param0 = arcpy.Parameter(
            displayName = 'BodyOfWater',
            name = 'input_fn',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Input')
# The first parameter is where the user will input the body of water shapefile they are examining
# The second paramter  is where the user will define the name and file location for the output of the buffer zones created
        # second parameter
        param1 = arcpy.Parameter(
            displayName = 'BufferName',
            name = 'BufferName',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Output')
#The third parameter is where the use will input the desired distances and sizes of the multiple zones around the bodies of water
        # third parameter
        param2 = arcpy.Parameter(
            displayName = 'Distances',
            name = 'Distances',
            datatype = 'Double',
            parameterType = 'Required',
            direction = 'Input', 
            multiValue=True)

        # return a list of parameters
        params = [param0, param1, param2]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        BodyOfWater = parameters[0].valueAsText
        BufferName = parameters[1].valueAsText
        Distances = parameters[2].values
        arcpy.MultipleRingBuffer_analysis(BodyOfWater, BufferName, Distances,"Meters", "Fieldname", "ALL")
        messages.addMessage('Thanks for a great semester!')
# A helpful message can be added here as an indication the tool ran properly
        return
